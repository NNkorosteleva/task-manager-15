package ru.tsc.korosteleva.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
