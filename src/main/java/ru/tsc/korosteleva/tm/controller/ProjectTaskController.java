package ru.tsc.korosteleva.tm.controller;

import ru.tsc.korosteleva.tm.api.controller.IProjectTaskController;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
    }
}
