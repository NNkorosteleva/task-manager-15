package ru.tsc.korosteleva.tm.component;

import ru.tsc.korosteleva.tm.api.controller.ICommandController;
import ru.tsc.korosteleva.tm.api.controller.IProjectController;
import ru.tsc.korosteleva.tm.api.controller.IProjectTaskController;
import ru.tsc.korosteleva.tm.api.controller.ITaskController;
import ru.tsc.korosteleva.tm.api.repository.ICommandRepository;
import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.repository.IProjectTaskRepository;
import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ICommandService;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.constant.ArgumentConst;
import ru.tsc.korosteleva.tm.constant.TerminalConst;
import ru.tsc.korosteleva.tm.controller.CommandController;
import ru.tsc.korosteleva.tm.controller.ProjectController;
import ru.tsc.korosteleva.tm.controller.ProjectTaskController;
import ru.tsc.korosteleva.tm.controller.TaskController;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.korosteleva.tm.exception.system.CommandNotSupportedException;
import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.model.Task;
import ru.tsc.korosteleva.tm.repository.CommandRepository;
import ru.tsc.korosteleva.tm.repository.ProjectRepository;
import ru.tsc.korosteleva.tm.repository.ProjectTaskRepository;
import ru.tsc.korosteleva.tm.repository.TaskRepository;
import ru.tsc.korosteleva.tm.service.CommandService;
import ru.tsc.korosteleva.tm.service.ProjectService;
import ru.tsc.korosteleva.tm.service.ProjectTaskService;
import ru.tsc.korosteleva.tm.service.TaskService;
import ru.tsc.korosteleva.tm.util.DateUtil;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskRepository projectTaskRepository = new ProjectTaskRepository(projectRepository, taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectTaskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void run(final String[] args) {
        if (processArgument(args)) close();
        initData();
        showProcess();
    }

    public void showProcess() {
        commandController.showWelcome();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID:
                taskController.showTaskListByProject();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_TO_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        try {
            processArgument(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("[FAIL]");
            close();
        }
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void close() {
        System.exit(0);
    }

    private void initData() {
        taskService.add(new Task("DEMO TASK", "DEMO DESCRIPTION", Status.NOT_STARTED, DateUtil.toDate("10.10.2019")));
        taskService.add(new Task("TEST TASK", "TEST DESCRIPTION", Status.IN_PROGRESS, DateUtil.toDate("10.12.2020")));
        taskService.add(new Task("CHECK TASK", "CHECK DESCRIPTION", Status.COMPLETED, DateUtil.toDate("20.01.2019")));
        taskService.add(new Task("CHECKED TASK", "CHECKED DESCRIPTION", Status.IN_PROGRESS, DateUtil.toDate("05.04.2019")));
        projectService.add(new Project("DEMO PROJECT", "DEMO DESCRIPTION", Status.NOT_STARTED, DateUtil.toDate("12.12.2020")));
        projectService.add(new Project("TEST PROJECT", "TEST DESCRIPTION", Status.COMPLETED, DateUtil.toDate("15.11.2020")));
        projectService.add(new Project("CHECK PROJECT", "CHECK DESCRIPTION", Status.IN_PROGRESS, DateUtil.toDate("10.10.2019")));
        projectService.add(new Project("CHECKED PROJECT", "CHECKED DESCRIPTION", Status.COMPLETED, DateUtil.toDate("10.07.2021")));
    }

}